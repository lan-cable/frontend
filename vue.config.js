const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: ["vuetify"],
  devServer: {
    proxy: {
      "/api": {
        target: "http://10.2.0.2:3000/",
        ws: true,
        changeOrigin: true,
      },
    },
  },
});
